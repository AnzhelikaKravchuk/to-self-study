## Для продолжения обучения рекомендуем:

- Пройти курс [Version Control with Git](https://learn.epam.com/detailsPage?id=601f195a-d408-4439-a16d-0630ed2a412e)
- Пройти курс [Основы программирования на примере C#. Часть 1](https://ulearn.me/Course/BasicProgramming/Kratkaya_spravka_pered_nachalom_69a2e121-e58f-4cd0-8221-7affb7dc796e)
- Пройти курс [Основы программирования на примере C#. Часть 2](https://ulearn.me/Course/BasicProgramming2/Steki_i_ocheredi_48016626-87ae-411d-ae97-f7a49e465dbc)
- Зарегистрироваться на сайте на https://www.codewars.com/ и [выполнить задания](https://www.codewars.com/kata/search/csharp?q=&&tags=Object-oriented%20Programming&beta=false&order_by=rank_id%20asc) с тегами `CLASSES`, `OBJECT-ORIENTED PROGRAMMING`, `DATA STRUCTURES`
- Прочесть [Programming C# 8.0. by Ian Griffiths. O'Reilly Media. 2019](https://www.oreilly.com/library/view/programming-c-80/9781492056805/) with [Code Listings](https://github.com/idg10/prog-cs-8-examples)
- Выполнить задание ["Файловый кабинет"](https://github.com/epam-dotnet-lab/file-cabinet-task)

<details><summary>Дополнительная литература и полезные ссылки</summary>  

- [C# Notes for Professionals book](https://books.goalkicker.com/CSharpBook/) (Free)    
- [C# documentation. Microsoft Documentation](https://docs.microsoft.com/en-us/dotnet/csharp/)            
- [C# 8.0 in a Nutshell. by Joseph Albahari, Eric Johannsen. 2020](https://www.oreilly.com/library/view/c-80-in/9781492051121/) with [Code Lisings](http://www.albahari.com/nutshell/code.aspx)   
- [C# 10 in a Nutshell by Joseph Albahari. 2022](https://www.oreilly.com/library/view/c-10-in/9781098121945/) with [Code Listings](http://www.albahari.com/nutshell/code.aspx)  

</details>
